
## Fashionproject

Analysis of the online store of women 's shoes.

## Environment

### OS

Ubuntu18.04 64 bit

### Git

```
$ sudo apt install git
$ cd /
$ git clone https://keinen87@bitbucket.org/keinen87/fashionproject.git
```

### Docker

Instructions from [article](https://www.digitalocean.com/community/tutorials/docker-ubuntu-18-04-1-ru).

```bash
$ sudo apt update
$ sudo apt install apt-transport-https ca-certificates curl software-properties-common
$ curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
$ sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
$ sudo apt update
$ sudo apt install docker-ce
$ sudo usermod -aG docker ${USER}
```

### Selenoid

Instructions from [article](https://4te.me/post/selenium-docker/).

```bash
$ curl -s https://aerokube.com/cm/bash | bash
$ ./cm selenoid start --vnc
```

### Python and requirements

```bash
$ sudo apt install python3 python3-venv
$ cd /fashionproject
$ python3 -m venv env
$ . env/bin/activate
(env) $ pip install -r requirements.txt
```

### Environment variables

```bash
$ cat /fashionproject.env 
BROWSER_NAME=firefox
VERSION=76.0
PLATFORM=LINUX
HOST=localhost
PORT=4444
```

## Create database

```bash
$ python manage.py migrate
```

## Systemd

Instructions from [article](https://www.hippolab.ru/systemdtimer-zapusk-zadach-po-raspisaniyu).

For example, we want to get common info at 00.00 and detail info at 3.00.

- Put follow into `/etc/systemd/system/get_common_data.service`:

```bash
[Unit]
Description=Script for get common shoes info

[Service]
ExecStart=/fashionproject/env/bin/python /fashionproject/manage.py get_common_data
```

- And create timer `/etc/systemd/system/get_common_data.timer` for service above with follow contents:

```bash
[Unit]
Description=Timer for script for get common shoes info

[Timer]
OnCalendar=00:00

[Install]
WantedBy=timers.target
```

- Put follow into `/etc/systemd/system/get_detail_data.service` 

```bash
[Unit]
Description=Script for get detail shoes info

[Service]
ExecStart=/fashionproject/env/bin/python /fashionproject/manage.py get_detail_data
```

- Create timer `/etc/systemd/system/get_detail_data.timer` for service above with follow contents:

```bash
[Unit]
Description=Timer for script for get detail shoes info

[Timer]
OnCalendar=03:00

[Install]
WantedBy=timers.target
```

- Finally start your timers:

```bash
$ sudo systemctl daemon-reload
$ systemctl enable get_common_data.timer
$ systemctl start get_common_data.timer
$ systemctl enable get_detail_data.timer
$ systemctl start get_detail_data.timer
```

## Logs

To monitor in real time see `/var/log`:

```bash
$ tail -f /var/log/get_common_data_18.05.2020_05\:02.log
```
