# Generated by Django 3.0.6 on 2020-06-09 10:54

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('shoes', '0005_auto_20200608_1136'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='shoecard',
            name='created',
        ),
    ]
