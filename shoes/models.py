from django.db import models


class ShoeCard(models.Model):

    sku_number = models.CharField(max_length=120)
    brand = models.CharField(max_length=120)
    product = models.CharField(max_length=120)
    url = models.CharField(max_length=120)

class ShoeDetail(models.Model):
    
    shoecard = models.OneToOneField(ShoeCard, related_name='shoedetails', on_delete=models.CASCADE)
    sold_out = models.BooleanField(default=False)
    color = models.CharField(max_length=120, blank=True)
    img_url = models.CharField(max_length=120)
    details = models.CharField(max_length=120)

class ShoePrice(models.Model):

    shoedetail = models.ForeignKey(ShoeDetail, related_name='shoeprices', on_delete=models.CASCADE)
    currency = models.CharField(max_length=3, default="USD", blank=True)
    price = models.DecimalField(decimal_places=2, max_digits=20)
    initial_price = models.DecimalField(decimal_places=2, max_digits=20, blank=True, null=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=False)

class ShoeSize(models.Model):

    shoedetail = models.ForeignKey(ShoeDetail, related_name='shoesizes',on_delete=models.CASCADE)
    m4 = models.BooleanField(default=False,blank=True)
    m4_5 = models.BooleanField(default=False,blank=True)
    m5 = models.BooleanField(default=False,blank=True)
    m5_5 = models.BooleanField(default=False,blank=True)
    m6 = models.BooleanField(default=False,blank=True)
    m6_5 = models.BooleanField(default=False,blank=True)
    m7 = models.BooleanField(default=False,blank=True)
    m7_5 = models.BooleanField(default=False,blank=True)
    m8 = models.BooleanField(default=False,blank=True)
    m8_5 = models.BooleanField(default=False,blank=True)
    m9 = models.BooleanField(default=False,blank=True)
    m9_5 = models.BooleanField(default=False,blank=True)
    m10 = models.BooleanField(default=False,blank=True)
    m10_5 = models.BooleanField(default=False,blank=True)
    m11 = models.BooleanField(default=False,blank=True)
    m11_5 = models.BooleanField(default=False,blank=True)
    m12 = models.BooleanField(default=False,blank=True)
    created = models.DateTimeField(auto_now=False, auto_now_add=False)



