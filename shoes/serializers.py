from rest_framework import serializers
from shoes.models import ShoeCard, ShoeDetail, ShoePrice, ShoeSize


class ShoeCardSerializer(serializers.ModelSerializer):
    
    class Meta:
        model = ShoeCard
        fields = (
            'id',
            'sku_number',
            'brand',
            'product',
            'url'
        )


class ShoeDetailSerializer(serializers.ModelSerializer):

    class Meta:
        model = ShoeDetail
        fields = (
            'sold_out',
            'details'
        )

class ShoePriceSerializer(serializers.ModelSerializer):

    class Meta:
        model = ShoePrice
        fields = (
            'currency',
            'price'
        ) 
