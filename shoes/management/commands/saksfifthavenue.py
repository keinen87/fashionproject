import logging
from datetime import datetime
from decimal import Decimal
from django.utils import timezone
import re
from selenium.common import exceptions
from shoes.models import ShoeCard, ShoeDetail, ShoePrice

SAKSFIFTHAVENUE_BRANDS = [
    'Balenciaga',
    'Bottega Veneta',
    'Burberry',
    'Chloé',
    'Christian Louboutin',
    'COACH',
    'Gucci',
    'Jimmy Choo',
    'Miu Miu'
]

def saksfifthavenue_get_pages_urls(webdriver, capabilities, host, port):
    all_exceptions = []
    pages_urls = []
    site_url = 'https://www.saksfifthavenue.com/Shoes/shop/_/N-52k0s7/Ne-6lvnb5'
    try:
        driver = webdriver.Remote(
            command_executor=f'http://{host}:{port}/wd/hub',
            desired_capabilities=capabilities
        )
        driver.get(site_url)
        driver.implicitly_wait(10)
    except Exception as ex:
        all_exceptions.append(ex)
        return all_exceptions
    try:
        total_pages = int(driver.find_element_by_class_name('pagination-count').text)
    except Exception as ex:
        all_exceptions.append(ex)
        return all_exceptions
    url_template = 'https://www.saksfifthavenue.com/Shoes/shop/_/N-52k0s7/Ne-6lvnb5?FOLDER%3C%3Efolder_id=2534374306624247&Nao='
    url_part = 0
    try:
        current_page_products = driver.find_elements_by_class_name('mainBlackText')
        url_part_increase = len(current_page_products)
    except Exception as ex:
        all_exceptions.append(ex)
        return all_exceptions
    driver.quit()
    for _ in range(1,total_pages+1):
        page_url = f'{url_template}{url_part}'
        pages_urls.append(page_url)
        url_part += url_part_increase
    return all_exceptions, pages_urls        

def saksfifthavenue_common_parse(page_url, webdriver, capabilities, host, port):
    all_exceptions = []
    try:
        driver = webdriver.Remote(
            command_executor=f'http://{host}:{port}/wd/hub',
            desired_capabilities=capabilities
        )
        driver.get(page_url)
        driver.implicitly_wait(5)
    except Exception as ex:
        all_exceptions.append(ex)
        return all_exceptions
    try:
        shoes_cards = driver.find_elements_by_class_name('product-text')
    except Exception as ex:
        all_exceptions.append(ex)
        return all_exceptions
    for shoe_card in shoes_cards:
        try:
            sku_number = shoe_card.find_element_by_class_name('sfa-product-designer-name-container').get_attribute('productcode')
        except Exception as ex:
            all_exceptions.append(ex)
            continue
        try:
            brand = shoe_card.find_element_by_class_name('product-designer-name').text
        except Exception as ex:
            all_exceptions.append(ex)
            continue
        try:
            product = shoe_card.find_element_by_class_name('product-description').text
        except Exception as ex:
            all_exceptions.append(ex)
            continue
        try:
            url = shoe_card.find_element_by_class_name('mainBlackText').get_attribute('href').split('?')[0]
        except Exception as ex:
            all_exceptions.append(ex)
            continue
        ShoeCard.objects.update_or_create(
            sku_number = sku_number,
            url = url,
            defaults = {
                'brand': brand,
                'product': product   
            }
        )
    driver.quit()
    return all_exceptions

def saksfifthavenue_detail_parse(shoe_card, webdriver, capabilities, host, port):
    all_exceptions = []
    try:
        driver = webdriver.Remote(
            command_executor=f'http://{host}:{port}/wd/hub',
            desired_capabilities=capabilities
        )
        driver.get(shoe_card.url)
        driver.implicitly_wait(10)
    except Exception as ex:
        all_exceptions.append(ex)
        print('Ошибка при загрузке страницы')
        return all_exceptions
    sold_out = False    
    try:
        color_html = driver.find_element_by_class_name('product-variant-attribute-label__selected-value').text
        if color_html:
            try:
                color = driver.find_element_by_class_name('product-variant-attribute-label__selected-value').text
            except Exception as ex:
                all_exceptions.append(ex)
                driver.quit(); print(f'{ex}{shoe_card.url}')  
                return all_exceptions      
        else:
            try:
                color_html = driver.find_element_by_class_name('product-variant-attribute-values')
                try:
                    hidden_colors_html = color_html.find_elements_by_class_name('is-visually-hidden')
                    colors = []
                    for hidden_color_html in hidden_colors_html:
                        colors.append(hidden_color_html.text)
                        color = ' '.join(colors)    
                except Exception as ex:
                    all_exceptions.append(ex)
                    driver.quit(); print(f'{ex}{shoe_card.url}')
                    return all_exceptions            
            except Exception as ex:
                all_exceptions.append(ex)
                driver.quit(); print(f'{ex}{shoe_card.url}')
                return all_exceptions                 
    except exceptions.NoSuchElementException as ex:
        sold_out = True
        color = ''
    try:
        img_url = driver.find_element_by_class_name('s7container--placeholder').get_attribute("src")
    except Exception as ex:
        all_exceptions.append(ex)
        driver.quit(); print(f'{ex}{shoe_card.url}')
        return all_exceptions
    try:
        details = driver.find_element_by_class_name('product-description').text
    except Exception as ex:
        all_exceptions.append(ex)
        driver.quit(); print(f'{ex}{shoe_card.url}')
        return all_exceptions
    try:
        driver.find_element_by_class_name('product-pricing__price')
    except Exception:
        sold_out = True        
    ShoeDetail.objects.update_or_create(
            shoecard = shoe_card,
            defaults = {
                'sold_out': sold_out,
                'color': color,
                'img_url': img_url,
                'details': details   
            }
        )
    driver.quit()
    return all_exceptions      

def saksfifthavenue_price_parse(shoe_card, webdriver, capabilities, host, port):
    all_exceptions = []
    try:
        driver = webdriver.Remote(
            command_executor=f'http://{host}:{port}/wd/hub',
            desired_capabilities=capabilities
        )
        driver.get(shoe_card.url)
        driver.implicitly_wait(10)
    except Exception as ex:
        all_exceptions.append(ex)
        return all_exceptions
    try:
        prices_html = driver.find_elements_by_class_name('product-pricing__price')
        currency = re.findall('\D{3}',prices_html[0].text)[0]
        if len(prices_html) > 1:
            if '-' in prices_html[0].text:
                initial_price = Decimal(''.join(re.findall('[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?',prices_html[0].text.split('-')[-1:][0])))
                price = Decimal(''.join(re.findall('[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?',prices_html[1].text.split('-')[-1:][0])))
            else:
                initial_price = Decimal(''.join(re.findall('[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?',prices_html[0].text)))
                price = Decimal(''.join(re.findall('[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?',prices_html[1].text)[0]))
        else:
            initial_price = None
            if '-' in prices_html[0].text:
                price = Decimal(''.join(re.findall('[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?',prices_html[1].text.split('-')[-1:][0])))
            else:
                price = Decimal(''.join(re.findall('[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?',prices_html[0].text)))   
    except Exception as ex:
        print(f'{ex} {shoe_card.url}')
        driver.quit()
        ShoeDetail.objects.update_or_create(
            shoecard = shoe_card,
            defaults = {
                'sold_out': True   
            }
        )
        return    
    ShoePrice.objects.create(
            shoedetail = shoe_card.shoedetails,
            currency = currency,
            price = price,
            initial_price = initial_price,
            created = timezone.now()   
        )
    driver.quit()    
    return all_exceptions