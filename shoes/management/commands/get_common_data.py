import json
import logging
import os
import sys
from datetime import datetime
from django.core.management.base import BaseCommand
from django.shortcuts import render
from django.utils import timezone
from dotenv import load_dotenv
from .macys import \
    macys_get_pages_urls, \
    macys_common_parse, \
    MACYS_BRANDS, \
    macys_detail_parse, \
    macys_price_parse    
from .saksfifthavenue import \
    saksfifthavenue_get_pages_urls, \
    saksfifthavenue_common_parse, \
    SAKSFIFTHAVENUE_BRANDS, \
    saksfifthavenue_detail_parse, \
    saksfifthavenue_price_parse    
from selenium import webdriver
from shoes.models import ShoeCard, ShoeDetail
load_dotenv()

def get_logger(logger_name):
    logger = logging.getLogger(logger_name)
    logger.setLevel(logging.INFO)
    handler = logging.FileHandler(f'/var/log/{logger_name}_{datetime.strftime(timezone.now(),"%d.%m.%Y_%H:%M")}.log')
    handler.setLevel(logging.INFO)
    handler_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(handler_format)
    logger.addHandler(handler)
    return logger

class Command(BaseCommand):

    def handle(self, *args, **options):
        browsername = os.getenv('BROWSER_NAME')
        version = os.getenv('VERSION')
        platform = os.getenv('PLATFORM')
        host = os.getenv('HOST')
        port = os.getenv('PORT')
        capabilities = {
            'browserName': browsername,
            'version': version,
            'platform': platform
        }
        
        get_common_data_logger = get_logger('get_common_data')
        get_common_data_logger.info('Start getting common data...')
        common_description_parsers = {
            saksfifthavenue_common_parse: saksfifthavenue_get_pages_urls,
            macys_common_parse: macys_get_pages_urls
        }
        for parser in common_description_parsers:
            parser_name = parser.__name__
            parser_logger = get_logger(parser_name)
            parser_logger.info(f'Parser {parser_name} has been start {datetime.strftime(timezone.now(),"%d.%m.%Y")} at {datetime.strftime(timezone.now(),"%H:%M")}')
            exceptions, pages = common_description_parsers[parser](webdriver,capabilities,host,port)
            if exceptions:
                get_common_data_logger.info('Errors:')
                for exception in exceptions:
                    get_common_data_logger.info(f'{exception}')
                    sys.exit()
            else:
                parser_exceptions = []
                for number, page in enumerate(pages, start=1):
                    parser_logger.info(f'{number} Processing {page}')
                    parser_exceptions.append(parser(page, webdriver, capabilities, host, port))
                for exception in parser_exceptions:
                    if exception:
                        parser_logger.info(f'{exception}')
                        get_common_data_logger.info(f'{parser_name}\n{exception}')
                        exit()
                parser_logger.info(f'All common description from {parser_name} successfully saved in database {datetime.strftime(timezone.now(),"%d.%m.%Y")} в {datetime.strftime(timezone.now(),"%H:%M")}')
        
        get_common_data_logger.info('End getting common data')
        
        get_detail_data_logger = get_logger('get_detail_data')
        get_detail_data_logger.info('Start getting detail data...')
        detail_description_parsers = {
            saksfifthavenue_detail_parse: SAKSFIFTHAVENUE_BRANDS,
            macys_detail_parse: MACYS_BRANDS
        }
        for parser in detail_description_parsers:
            parser_name = parser.__name__
            parser_logger = get_logger(parser_name)
            parser_logger.info(f'Parser {parser_name} has been start {datetime.strftime(timezone.now(),"%d.%m.%Y")} at {datetime.strftime(timezone.now(),"%H:%M")}')
            url_part = parser_name.split('_')[0]
            shoe_cards = ShoeCard.objects.filter(
                brand__in=detail_description_parsers[parser],
                url__contains=url_part
            )
            parser_exceptions = []
            for number, shoe_card in enumerate(shoe_cards, start=1):
                parser_logger.info(f'{number} Processing {shoe_card.url}')
                parser_exceptions.append(parser(shoe_card, webdriver, capabilities, host, port))
            for exception in parser_exceptions:
                if exception:
                    parser_logger.info(f'{exception}')
                    get_detail_data_logger.info(f'{parser_name}\n{exception}')
                    sys.exit()
            parser_logger.info(f'All detail description from {parser_name} successfully saved in database {datetime.strftime(timezone.now(),"%d.%m.%Y")} в {datetime.strftime(timezone.now(),"%H:%M")}')    
        get_detail_data_logger.info('End getting detail data')
        
        get_price_data_logger = get_logger('get_price_data')
        get_price_data_logger.info('Start getting price data...')
        price_parsers = [
            saksfifthavenue_price_parse,
            macys_price_parse
        ]
        for parser in price_parsers:
            parser_name = parser.__name__
            parser_logger = get_logger(parser_name)
            parser_logger.info(f'Parser {parser_name} has been start {datetime.strftime(timezone.now(),"%d.%m.%Y")} at {datetime.strftime(timezone.now(),"%H:%M")}')
            url_part = parser_name.split('_')[0]
            shoe_detail_cards = ShoeDetail.objects.filter(
                sold_out=False,
                shoecard__url__contains=url_part
            )
            parser_exceptions = []
            for number, shoe_detail_card in enumerate(shoe_detail_cards, start=1):
                parser_logger.info(f'{number} Processing {shoe_detail_card.shoecard.url}')
                parser_exceptions.append(parser(shoe_detail_card.shoecard, webdriver, capabilities, host, port))
            for exception in parser_exceptions:
                if exception:
                    parser_logger.info(f'{exception}')
                    sys.exit()
            parser_logger.info(f'All price description from {parser_name} successfully saved in database {datetime.strftime(timezone.now(),"%d.%m.%Y")} в {datetime.strftime(timezone.now(),"%H:%M")}')
        get_price_data_logger.info('End getting price data')
        