import json
import logging
import os
import re
from datetime import datetime
from datetime import timedelta
from decimal import Decimal
from django.core.management.base import BaseCommand
from django.shortcuts import render
from django.utils import timezone
from dotenv import load_dotenv
from selenium import webdriver
from selenium.common import exceptions
from shoes.models import ShoeCard, ShoeDetail, ShoePrice, ShoeSize, ShoeSize
load_dotenv()


class Command(BaseCommand):

    def handle(self, *args, **options):
        brands = [
            'Balenciaga',
            'Bottega Veneta',
            'Burberry',
            'Chloé',
            'Christian Louboutin',
            'COACH',
            'Gucci',
            'Jimmy Choo',
            'Miu Miu'
        ]
        logging.basicConfig(filename=f'/var/log/get_detail_data_{datetime.strftime(timezone.now(),"%d.%m.%Y_%H:%M")}.log', filemode='w', level=logging.INFO)
        browsername = os.getenv("BROWSER_NAME")
        version = os.getenv("VERSION")
        platform = os.getenv("PLATFORM")
        host = os.getenv("HOST")
        port = os.getenv("PORT")
        shoe_cards = ShoeCard.objects.filter(
            created__day=timezone.now().day,
            brand__in=brands
        )
        #shoe_cards_detail = ShoeDetail.objects.filter(shoecard__in=shoe_cards)
        if len(shoe_cards) == 0:
            logging.info('Не собрана общая информация. Запустите get_common_data')
            exit()
        else:
            logging.info(f'Парсер запущен {datetime.strftime(timezone.now(),"%d.%m.%Y")} в {datetime.strftime(timezone.now(),"%H:%M")}')
            capabilities = {
                    'browserName': browsername,
                    'version': version,
                    'platform': platform
                }
            for number, shoe_card in enumerate(shoe_cards, start=1):
                logging.info(f'{number}. Обрабатывается {shoe_card.url}')
                try:
                    driver = webdriver.Remote(
                                command_executor=f'http://{host}:{port}/wd/hub',
                                desired_capabilities=capabilities
                            )
                    driver.get(shoe_card.url)
                    driver.implicitly_wait(10)
                except Exception as ex:
                    logging.error(f'Ошибка драйвера\n{ex}')
                    exit()
                sold_out = False
                sizes = []
                size_list_html = driver.find_elements_by_class_name('product-variant-attribute-value--text')
                if size_list_html:
                    for size_text in size_list_html:
                        sizes.append(size_text.find_element_by_tag_name('span').text)
                    size = ' '.join(sizes)
                else:
                    try:
                        size_drop_down_html = driver.find_element_by_class_name('drop-down-list')
                        size_html = size_drop_down_html.find_elements_by_tag_name('option')
                        sizes = [size.text for size in size_html]
                        size = ' '.join(sizes[1:])
                    except exceptions.NoSuchElementException as ex:
                        logging.info(f'Не указан размер или ошибка вычисления размера\n{ex}')
                        sold_out = True
                try:
                    color_html = driver.find_element_by_class_name('product-variant-attribute-label__selected-value').text
                    if color_html:
                        try:
                            color_text = driver.find_element_by_class_name('product-variant-attribute-label__selected-value').text
                            color = color_text
                        except Exception as ex:
                                logging.error(f'Ошибка вычисления цвета\n{ex}')    
                    else:
                        try:
                            color_html = driver.find_element_by_class_name('product-variant-attribute-values')
                            try:
                                hidden_colors_html = color_html.find_elements_by_class_name('is-visually-hidden')
                                colors = []
                                for hidden_color_html in hidden_colors_html:
                                    colors.append(hidden_color_html.text)
                                    color = ' '.join(colors)    
                            except Exception as ex:
                                logging.error(f'Ошибка вычисления цвета\n{ex}')            
                        except Exception as ex:
                            logging.error(f'Ошибка вычисления цвета\n{ex}')
                    try:
                        prices_html = driver.find_elements_by_class_name('product-pricing__price')
                        currency = re.findall('\D{3}',prices_html[0].text)[0]
                        if len(prices_html) > 1:
                            initial_price = Decimal(re.findall('[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?',prices_html[0].text)[0])
                            price = Decimal(re.findall('[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?',prices_html[1].text)[0])
                        else:
                            initial_price = None
                            price = Decimal(re.findall('[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?',prices_html[0].text)[0])   
                    except Exception as ex:
                        logging.error(f'Ошибка вычисления цены\n{ex}')
                        sold_out = True                  
                except exceptions.NoSuchElementException:
                    sold_out = True
                try:
                    img_url = driver.find_element_by_class_name('s7container--placeholder').get_attribute("src")
                except Exception as ex:
                    logging.error(f'Ошибка вычисления ссылки на изображение\n{ex}')
                try:
                    details = driver.find_element_by_class_name('product-description').text
                except Exception as ex:
                    logging.error(f'Ошибка вычисления детальной информации\n{ex}')
                shoe_detail = ShoeDetail.objects.create(
                    shoecard = shoe_card,
                    sold_out = sold_out,
                    color = color,
                    img_url = img_url,
                    details = details
                )           
                shoe_detail.save()
                shoe_price = ShoePrice.objects.create(
                    shoedetail = shoe_detail,
                    currency = currency,
                    price = price,
                    initial_price = initial_price,
                    created = timezone.now() 
                )
                shoe_price.save()
                m4 = m4_5 = m5 = m5_5 = m6 = m6_5 = m7 = m7_5 = m8 = m8_5 = m9 = m9_5 = m10 = m10_5 = m11 = m11_5 = m12 = False
                if '4' in size:
                    m4 = True
                if '4.5' in size:
                    m4_5 = True
                if '5' in size:
                    m5 = True
                if '5.5' in size:
                    m5_5 = True
                if '6' in size:
                    m6 = True
                if '6.5' in size:
                    m6_5 = True
                if '7' in size:
                    m7 = True
                if '7.5' in size:
                    m7_5 = True
                if '8' in size:
                    m8 = True
                if '8.5' in size:
                    m8_5 = True
                if '9' in size:
                    m9 = True
                if '9.5' in size:
                    m9_5 = True
                if '10' in size:
                    m10 = True
                if '10.5' in size:
                    m10_5 = True
                if '11' in size:
                    m11 = True
                if '11.5' in size:
                    m11_5 = True
                if '12' in size:
                    m12 = True                                                                    
                shoe_size = ShoeSize.objects.create(
                    shoedetail = shoe_detail,
                    m4 = m4,
                    m4_5 = m4_5,
                    m5 = m5,
                    m5_5 = m5_5,
                    m6 = m6,
                    m6_5 = m6_5,
                    m7 = m7,
                    m7_5 = m7_5,
                    m8 = m8,
                    m8_5 = m8_5,
                    m9 = m9,
                    m9_5 = m9_5,
                    m10 = m10,
                    m10_5 = m10_5,
                    m11 = m11,
                    m11_5 = m11_5,
                    m12 = m12,
                    created = timezone.now()
                )
                shoe_size.save()
                driver.quit()
            logging.info(f'Данные успешно сохранены в базу {datetime.strftime(timezone.now(),"%d.%m.%Y")} в {datetime.strftime(timezone.now(),"%H:%M")}')
    
