import json
import logging
import re
from datetime import datetime
from decimal import Decimal
from django.utils import timezone
from shoes.models import ShoeCard, ShoeDetail, ShoePrice, ShoeSize

MACYS_BRANDS = [
    'Cole Haan',
    'INC International Concepts'
]

def macys_get_pages_urls(webdriver, capabilities, host, port):
    all_exceptions = []
    pages_urls = []
    site_url = 'https://www.macys.com/shop/shoes/all-womens-shoes?id=56233&edge=hybrid'
    try:
        driver = webdriver.Remote(
            command_executor=f'http://{host}:{port}/wd/hub',
            desired_capabilities=capabilities
        )
        driver.get(site_url)
        driver.implicitly_wait(10)
    except Exception as ex:
        all_exceptions.append(ex)
    total_pages = int(driver.find_element_by_id('select-page').find_element_by_tag_name('option').text.split(' ')[3])
    driver.quit()
    for page in range(1, total_pages+1):
        page_url = f'https://www.macys.com/shop/shoes/all-womens-shoes/Pageindex/{page}?id=56233'
        pages_urls.append(page_url)  
    return all_exceptions, pages_urls

def macys_common_parse(page_url, webdriver, capabilities, host, port):
    all_exceptions = []
    try:
        driver = webdriver.Remote(
        command_executor=f'http://{host}:{port}/wd/hub',
        desired_capabilities=capabilities
        )    
        driver.get(page_url)
        driver.implicitly_wait(10)
    except Exception as ex:
        all_exceptions.append(ex)
    try:
        shoe_cards = driver.find_elements_by_css_selector('li.productThumbnailItem')
    except Exception as ex:
        all_exceptions.append(ex)
    for shoe_card in shoe_cards:
        try:
            sku_number = shoe_card.find_element_by_css_selector('div.productThumbnail').get_attribute('id')
        except Exception as ex:
            all_exceptions.append(ex)
        try:
            brand = shoe_card.find_element_by_class_name('productBrand').text
        except Exception as ex:
            all_exceptions.append(ex)
        try:
            product = shoe_card.find_element_by_tag_name('a').get_attribute('title')
        except Exception as ex:
            all_exceptions.append(ex)
        try:
            url = shoe_card.find_element_by_tag_name('a').get_attribute('href')
        except Exception as ex:
            all_exceptions.append(ex)
        ShoeCard.objects.update_or_create(
            sku_number = sku_number,
            url = url,
            defaults = {
                'brand': brand,
                'product': product   
            }
        )        
    driver.quit()
    return all_exceptions

def macys_detail_parse(shoe_card, webdriver, capabilities, host, port):
    all_exceptions = []
    try:
        driver = webdriver.Remote(
            command_executor=f'http://{host}:{port}/wd/hub',
            desired_capabilities=capabilities
        )
        driver.implicitly_wait(10)
        driver.get(shoe_card.url)
    except Exception as ex:
        all_exceptions.append(ex); print('Driver Error')
        return all_exceptions
    sold_out = False    
    try:
        color_html = driver.find_element_by_class_name('swatch-webp')
        color = color_html.get_attribute('src')
    except Exception as ex:
        sold_out = True
        color = ''
    try:
        shoe_data = driver.find_element_by_id('productMktData')
        item = shoe_data.get_attribute('innerHTML')
        item_dict = json.loads(item)
        img_url = item_dict['image']
    except Exception as ex:
        sold_out = True
        img_url = ''           
    try:
        details = driver.find_element_by_class_name('accordion-body').text
    except Exception as ex:
        #all_exceptions.append(f'{shoe_card.url}\n{ex}')
        driver.quit()
        #print(f'{shoe_card.url}\n{ex}')
        return all_exceptions
    ShoeDetail.objects.update_or_create(
        shoecard = shoe_card,
        defaults = {
            'sold_out': sold_out,
            'color': color,
            'img_url': img_url,
            'details': details   
        }
    )
    driver.quit()
    return all_exceptions

def macys_price_parse(shoe_card, webdriver, capabilities, host, port):
    all_exceptions = []
    try:
        driver = webdriver.Remote(
            command_executor=f'http://{host}:{port}/wd/hub',
            desired_capabilities=capabilities
        )
        driver.get(shoe_card.url)
        driver.implicitly_wait(10)
    except Exception as ex:
        all_exceptions.append(ex)
        return all_exceptions
    sold_out = False    
    try:
        shoe_data = driver.find_element_by_id('productMktData')
        item = shoe_data.get_attribute('innerHTML')
        item_dict = json.loads(item)
        price = Decimal(item_dict['offers'][0]['price'])
    except Exception:
        sold_out = True
    try:
        prices_html = driver.find_elements_by_class_name('price')
        for price_html in prices_html:
            if price_html.text:
                price_text = price_html.text
        currency = re.findall('[A-Z]{3}', price_text)
        initial_price = Decimal(''.join(re.findall('[-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?', price_text)))         
    except Exception:
        sold_out = True
    if sold_out:
        driver.quit()
        ShoeDetail.objects.update_or_create(
            shoecard = shoe_card,
            defaults = {
                'sold_out': True   
            }
        )
        return
    else:
        ShoePrice.objects.create(
                shoedetail = shoe_card.shoedetails,
                currency = currency,
                price = price,
                initial_price = initial_price,
                created = timezone.now()   
            )
    driver.quit()    
    return all_exceptions
