from django.shortcuts import get_object_or_404
from rest_framework import viewsets
from rest_framework.response import Response
from shoes.management.commands.saksfifthavenue import SAKSFIFTHAVENUE_BRANDS
from shoes.management.commands.macys import MACYS_BRANDS
from shoes.models import ShoeCard, ShoeDetail, ShoePrice
from shoes.serializers import ShoeCardSerializer, ShoePriceSerializer

BRANDS = []
BRANDS.extend(SAKSFIFTHAVENUE_BRANDS)
BRANDS.extend(MACYS_BRANDS)

class ShoeCardView(viewsets.ViewSet):

    def list(self, request):
        queryset = ShoeCard.objects.filter(
            brand__in=BRANDS,
            shoedetails__sold_out=False
        )
        serializer = ShoeCardSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = ShoeCard.objects.filter(
            brand__in=BRANDS,
            shoedetails__sold_out=False
        )()
        user = get_object_or_404(queryset, pk=pk)
        serializer = ShoeCardSerializer(user)
        return Response(serializer.data)

class ShoePriceView(viewsets.ViewSet):

    def list(self, request):
            queryset = ShoePrice.objects.all()
            serializer = ShoePriceSerializer(queryset, many=True)
            return Response(serializer.data)
    